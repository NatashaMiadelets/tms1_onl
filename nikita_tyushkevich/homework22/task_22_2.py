from selenium import webdriver


def test_exercise():
    first_name = 'Nikita'
    last_name = "Tyushkevich"
    us_name = "InvisibleNever"
    link = 'http://demo.guru99.com/test/newtours/register.php'
    driver = webdriver.Chrome('./chromedriver.exe')
    driver.implicitly_wait(5)
    driver.get(link)
    fn = driver.find_element_by_name('firstName')
    fn.send_keys(first_name)
    ln = driver.find_element_by_name('lastName')
    ln.send_keys(last_name)
    phone = driver.find_element_by_name('phone')
    phone.send_keys('+375295050038')
    mail = driver.find_element_by_id('userName')
    mail.send_keys('sherlocktrue@domain.com')
    address = driver.find_element_by_name('address1')
    address.send_keys('Øvre Storgate 1B')
    city = driver.find_element_by_name('city')
    city.send_keys('Drammen')
    state = driver.find_element_by_name('state')
    state.send_keys('Norge')
    zip_code = driver.find_element_by_name('postalCode')
    zip_code.send_keys('3018')
    country = driver.find_element_by_name('country')
    country.click()
    select_country = driver.find_element_by_xpath('//option[@value="NORWAY"]')
    select_country.click()
    username = driver.find_element_by_id('email')
    username.send_keys(us_name)
    password = driver.find_element_by_name('password')
    password.send_keys('pass@w0rd')
    conf_password = driver.find_element_by_name('confirmPassword')
    conf_password.send_keys('pass@w0rd')
    submit = driver.find_element_by_name('submit')
    submit.click()
    name = driver.find_element_by_xpath('//table//tr[3]//p[1]/font/b')
    user = driver.find_element_by_xpath('//table//tr[3]//p[3]/font/b')
    assert f"{first_name} {last_name}" in name.text and f"{us_name}"\
           in user.text
    driver.quit()
