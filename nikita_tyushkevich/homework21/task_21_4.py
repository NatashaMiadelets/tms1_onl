# Work with yaml

import yaml
import json

with open("order.yaml") as f:
    data = yaml.safe_load(f)

# Print order number and address
print("Invoice: ", data["invoice"])
print("Address: ", data["bill-to"]["address"])

# Print description, price and quantity
for i in data["product"]:
    desc, price, qty = i["description"], i["price"], i["quantity"]
    print(f"Product description: {desc}"
          f"\nPrice: {price}"
          f"\nQuantity: {qty}")

# Convert yaml to json
with open('order.json', 'w') as j:
    json.dump(str(data), j)

# Create yaml file
title = 'Spyfall 2'
release_date = 2017
game_type = 'Party game'
players_qty = '4 - 12'
description = 'Spyfall is played over several rounds, in different locations'
title_2 = 'Eldritch Horror'
release_date_2 = 2013
game_type_2 = 'Survival horror'
players_qty_2 = '1 - 8'
description_2 = 'Cooperative fight against an ancient evil'

boardgame = {'Game_1': [title, release_date, game_type, players_qty,
                        description],
             'Game_2': [title_2, release_date_2, game_type_2,
                        players_qty_2, description_2]}

with open('boardgame.yaml', 'w') as spy:
    yaml.dump(boardgame, spy)
