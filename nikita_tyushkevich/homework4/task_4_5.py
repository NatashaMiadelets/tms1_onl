# Объединение словарей
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
c = {**a, **b}
for k in c.keys():
    if k in a and k in b:
        c[k] = [a[k], b[k]]
    elif k in b:
        c[k] = [None, b[k]]
    else:
        c[k] = [a[k], None]
print(c)
