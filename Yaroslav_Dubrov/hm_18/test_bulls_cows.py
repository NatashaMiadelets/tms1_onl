from bulls_cows import game_part
import unittest


class TestStrings(unittest.TestCase):
    def test_positive_one(self):
        self.assertEqual(game_part([1, 2, 3, 4], 1234), (4, 4))

    def test_positive_two(self):
        self.assertEqual(game_part([5, 3, 8, 7], 5234), (2, 1))

    def test_positive_three(self):
        self.assertEqual(game_part([9, 7, 5, 2], 1463), (0, 0))

    @unittest.expectedFailure
    def test_negative_double_digits(self):
        self.assertEqual(game_part([9, 7, 5, 2], 9772), (3, 3))
