# import random

# Generating PC number with unique numbers - don't need this in test
'''j = 10
pc = []
while j > 0:
    r = random.randint(1, 9)
    if r not in pc:
        pc.append(r)
    if len(pc) >= 4:
        break
    j -= 1

print(f"Only for debugging purposes! PC number is: {pc}")
'''

# Game part


def game_part(pc, u_input):
    # user = []

    # while pc != user:
    # u_input = input("Please enter your 4 digits number: ")

    # Convert user number to list
    user = list(map(int, str(u_input)))

# Finding cows as pairs of duplicate values in pc and user lists
    res = [x for x in pc + user if x in pc and x in user]

# Count of pairs in res list = count of cows
    if len(res) == 2:
        cows = 1
        print("Cows count: 1")
    elif len(res) == 4:
        cows = 2
        print("Cows count: 2")
    elif len(res) == 6:
        cows = 3
        print("Cows count: 3")
    elif len(res) == 8:
        cows = 4
        print("Cows count: 4")
    else:
        cows = 0
        print("Cows count: 0")

# Finding bulls compiling list with matched by index values
    bulls_preview = []
    for i in range(len(pc)):
        if pc[i] == user[i]:
            bulls_preview.append(pc[i])
    bulls = len(bulls_preview)
    print(f"Bulls count: {bulls}")
    return cows, bulls

    '''else:
        print("You win!")'''
