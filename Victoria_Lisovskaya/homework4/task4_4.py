list1 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
# insert new value on position = 3 (suppose that position starts with 1)
if len(list1) > 1:
    list1.insert(2, "test")
    print(list1)
# delete value with index = 6
if len(list1) > 5:
    del list1[6]
    print(list1)
