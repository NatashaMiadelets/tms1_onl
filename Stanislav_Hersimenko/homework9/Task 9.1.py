class Library:

    def __init__(self, title, author, pages, ISBN,
                 reserved=True, ID=True):
        self.title = title
        self.author = author
        self.pages = pages
        self.ISBN = ISBN
        self.reserved = reserved
        self.ID = ID


class User:
    def __init__(self, user_name, book=True):
        self.book = book
        self.user_name = user_name

    def book_taken(self, book):
        if book.reserved:
            book.reserved = False
            print(self.user_name, f"взял книгу '{book.title}'")
        else:
            print(f"Книга '{book.title}' недоступна")

    def book_return(self, book):
        print(f"'{book.title}' возвращена пользователем", self.user_name)

    def book_reserved(self, book):
        if book.reserved:
            book.reserved = False
            print(f"{book.ISBN} забронирована пользователем", self.user_name)
        else:
            print(f"{book.ISBN} 2006 года забронирована другим пользователем."
                  f" Доступно издание{book4.ISBN} 2010 года")


User1 = User("Чайник")
User2 = User("Шланг")
User3 = User("Баклан")

book1 = Library("Возвышение Хоруса", "Дэн Абнетт", "448", "5911811162")
book2 = Library("Лживые боги", "Грэм Макнилл", "416", "9781844163700")  # 2006
book4 = Library("Лживые боги", "Грэм Макнилл", "416", "9780857870223")  # 2010
book3 = Library("Галактика в огне", "Бен Каунтер", "416", "9781844163939")

User1.book_taken(book1)
User2.book_taken(book1)
User2.book_reserved(book2)
User3.book_reserved(book2)
User3.book_return(book3)
