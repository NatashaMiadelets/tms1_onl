class Investing:

    def __init__(self, N, R):
        self.N = N
        self.R = R

    def deposit(self):
        time_range = self.R * 12
        for i in range(time_range):
            self.N = self.N + self.N * 0.10 / 12
        print(f"Сумма: {int(self.N)} BYN")


result = Investing(1200, 1)
result.deposit()
