from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import pytest


@pytest.fixture(scope="function")
def start_test():
    url = "http://the-internet.herokuapp.com/dynamic_controls"
    path = "C:/chromedriver.exe"
    driver = webdriver.Chrome(path)
    driver.implicitly_wait(5)
    driver.get(url)
    yield driver
    driver.quit()


def test_checkbox(start_test):
    driver = start_test
    checkbox = driver.find_element_by_xpath('//*[@id="checkbox"]/input')
    checkbox.click()
    Remove = driver.find_element_by_xpath('//*[@id="checkbox-example"]/button')
    Remove.click()
    text = driver.find_element_by_xpath('//*[@id="message"]').text
    assert text == "It's gone!"
    try:
        driver.find_element_by_xpath('//*[@id="checkbox"]/input')
    except NoSuchElementException:
        return False
    return True


def test_input(start_test):
    driver = start_test
    input = driver.find_element_by_xpath('//*[@id="input-example"]/input')
    if input.is_enabled() is not True:
        pass
    button = driver.find_element_by_xpath('//*[@id="input-example"]/button')
    button.click()
    text = driver.find_element_by_xpath('//*[@id="message"]').text
    assert text == "It's enabled!"
    if input.is_enabled():
        input.click()
