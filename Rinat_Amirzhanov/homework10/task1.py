class Flower:
    def __init__(self, name, color, stem_length, price, days):
        self.name = name
        self.color = color
        self.stem_length = stem_length
        self.price = price
        self.days = days


class Bouquet:

    def __init__(self):
        self.flowers = []

    def add_flowers(self, *flowers):
        for flower in flowers:
            self.flowers.append(flower)

    def find_flower(self, name):
        flower_name = [flower.name for flower in self.flowers]
        if name in flower_name:
            return "Цветок есть в букете"
        else:
            return "Такого цветка нет в букете"

    def get_total_price(self):
        flowers_price = sum([flower.price for flower in self.flowers])
        return f"Цена за букет: {flowers_price} KZT"

    def determine_withering_period(self):
        lifetime = sum([flower.days for flower in self.flowers])
        withering_period = round(lifetime / len(self.flowers), 2)
        return f"Время увядания букета: {withering_period} (суток)"

    def sort_by_param(self, param):
        sorted_flowers = sorted(self.flowers, key=lambda x: getattr(x, param))
        flower_name = [flower.name for flower in sorted_flowers]
        return f"Сортировка цветов по параметру '{param}': {flower_name}"

    def find_by_param(self, param, value):
        search_result = [flower.name for flower in self.flowers
                         if getattr(flower, param) == value]
        return f"найдено: {search_result}"


rose = Flower("Роза", "красный", 10, 2000, 9)
carnation = Flower("Гвоздика", "зеленый", 8, 3000, 7)
chamomile = Flower("Ромашка", "желтый", 9, 1500, 8)

bouquet = Bouquet()

bouquet.add_flowers(rose, carnation, chamomile)

print(bouquet.find_by_param("price", 1500))
print(bouquet.find_flower("Тюльпан"))
print(bouquet.find_flower("Гвоздика"))
print(bouquet.get_total_price())
print(bouquet.determine_withering_period())
print(bouquet.sort_by_param("color"))
print(bouquet.sort_by_param("stem_length"))
