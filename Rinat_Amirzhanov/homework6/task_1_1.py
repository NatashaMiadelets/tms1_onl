import string


def check_card():
    while True:
        card_number = input("Введите карту: ")
        if len(card_number) < 16:
            print("Номер карты должен состоять из 16 цифр")
            print("False")
            continue
        elif len(card_number) > 16:
            print("Номер карты должен состоять из 16 цифр")
            print("False")
            continue
        for condition in card_number:
            if condition != "" and condition not in string.punctuation \
                    and condition.isdigit() is False:
                print("False")
                continue
        card_number = list(map(int, card_number))[::-1]
        for i in range(1, len(card_number), 2):
            if card_number[i] < 5:
                card_number[i] = card_number[i] * 2
            else:
                card_number[i] = ((card_number[i] * 2) // 10) +\
                                 ((card_number[i] * 2) % 10)
        sum_card_number = sum(card_number)
        if sum_card_number % 10 != 0:
            print("False")
        else:
            print("True")
        break


check_card()
