from unittest import TestCase
from unittest import expectedFailure
from program import text


class TestWords(TestCase):

    def test_word_without_duplicate(self):
        actual = text("abcde")
        expected = text("abcde")
        self.assertEqual(actual, expected)

    def test_duplicate_word_in_first_position(self):
        actual = text("cccba")
        expected = text("c3ba")
        self.assertEqual(actual, expected)

    def test_duplicate_word_in_last_position(self):
        actual = text("abdefffff")
        expected = text("abdef5")
        self.assertEqual(actual, expected)

    def test_word_multiple_duplicate(self):
        actual = text("abeehhhhhccced")
        expected = text("abe2h5c3ed")
        self.assertEqual(actual, expected)

    @expectedFailure
    def test_word_negative(self):
        actual = text("aaabbceedd")
        expected = text("a3bce2d2")
        self.assertEqual(actual, expected)
