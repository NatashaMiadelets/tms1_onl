from dina_shunkina.hm_25.pages.main_page import MainPage
from dina_shunkina.hm_25.pages.search_page import SearchPage


def test_user_search_item(browser):
    main_page = MainPage(browser)
    main_page.open_page()
    main_page.should_be_signin_page()
    main_page.search_item("dress")
    search_page = SearchPage(browser)
    search_page.should_be_search_page()
