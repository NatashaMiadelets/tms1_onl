from selenium import webdriver
from time import sleep
browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/filling-out-forms/")

try:
    name_field = browser.find_element_by_id("et_pb_contact_name_1")
    name_field.send_keys("Dina")
    message_field = browser.find_element_by_xpath(
        "//textarea[@name='et_pb_contact_message_1']")
    message_field.send_keys("Welcome to us")
    input_number = browser.find_element_by_class_name(
        "input.et_pb_contact_captcha")
    input_number.send_keys("8")
    button_submit = browser.find_element_by_css_selector(
        ".et_contact_bottom_container :nth-child(2)[type='submit']")
    button_submit.click()
    sleep(3)
    checking = browser.find_element_by_css_selector("#et_pb_contact_form_1")
    assert checking.text == "Thanks for contacting us"
finally:
    browser.quit()
