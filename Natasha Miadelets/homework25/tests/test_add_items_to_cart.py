from pages.main_page import MainPage
from pages.cart_page import CartPage


def test_add_items_to_cart(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.add_item_to_cart()
    main_page.open_cart_page()
    cart_page = CartPage(browser)
    cart_page.should_be_cart_page()
    cart_page.check_that_item_added()
