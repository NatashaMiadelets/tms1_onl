from pages.main_page import MainPage
from pages.cart_page import CartPage


def test_check_empty_cart(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.should_be_main_page()
    main_page.open_cart_page()
    cart_page = CartPage(browser)
    cart_page.should_be_cart_page()
    cart_page.check_that_cart_empty()
