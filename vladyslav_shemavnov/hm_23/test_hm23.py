from selenium import webdriver
from selenium.webdriver.common.by import By

browser = webdriver.Chrome()
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/complicated-page/")


button_class_name = browser.find_element(By.CLASS_NAME, "et_pb_button_4")
button_class_name.click()

buttom_xpath = browser.find_element(By.XPATH,
                                    "//div[@id='main-content']//article"
                                    "//div//div//div//div"
                                    "//div[3]//div[2]//div[2]//a")
buttom_xpath.click()

button_css_selector = browser.find_element_by_css_selector(".et_pb_button_4")
button_css_selector.click()
