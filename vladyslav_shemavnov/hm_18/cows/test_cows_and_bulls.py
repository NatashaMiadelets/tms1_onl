from lesson5 import algorithm_for_game
import unittest


class TestNumbersOfLetters(unittest.TestCase):

    def test_first_positive(self):
        self.assertTrue(algorithm_for_game(number_for_game="3245",
                                           correct_answer="3254"), [2, 2])

    def test_second_positive(self):
        self.assertTrue(algorithm_for_game(number_for_game="4576",
                                           correct_answer="7654"), [4])

    def test_third_positive(self):
        self.assertTrue(algorithm_for_game(number_for_game="4576",
                                           correct_answer="4561"), [1, 2])

    @unittest.expectedFailure
    def test_first_negative(self):
        self.assertFalse(algorithm_for_game(number_for_game="8907",
                                            correct_answer="8054"), [2, 2])
