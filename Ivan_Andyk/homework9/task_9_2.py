class Investment:
    def __init__(self, money, year, month):
        self.money = money
        self.year = year
        self.month = month


class Bank(Investment):
    def deposit(self):
        period = ((self.year * 12) + self.month)
        for i in range(period):
            self.money += self.money * 0.10 / 12
        print(f"Сумма вклада: {int(self.money)}")


bank = Bank(1000, 1, 12)
bank.deposit()
